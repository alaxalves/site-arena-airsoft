FROM ruby:2.4.4

MAINTAINER andre de sousa

RUN apt-get update -qq && apt-get install -y apt-utils build-essential libpq-dev nodejs
RUN mkdir /arena-api

WORKDIR /arena-api

COPY Gemfile /arena-api/Gemfile

COPY Gemfile.lock /arena-api/Gemfile.lock
COPY . /arena-api

RUN bundle install 

RUN bundle exec rails db:create
RUN bundle exec rails db:migrate
RUN bundle exec rails db:seed
